import React from 'react';
import {
    createAppContainer,
    createSwitchNavigator
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import AccountScreen from './src/screens/AccountScreen';
import SigninScreen from './src/screens/SigninScreen';
import SignupScreen from './src/screens/SignupScreen';
import TrackCreateScreen from './src/screens/TrackCreateScreen';
import TrackDetailScreen from './src/screens/TrackDetailScreen';
import TrackListScreen from './src/screens/TrackListScreen';
import { setNavigator } from './src/navigationRef';
import LoadingScreenAuth from './src/screens/LoadingScreenAuth';
import { Provider as LocationProvider } from './src/context/LocationContext';
import { Provider as AuthProvider } from './src/context/AuthContext';
import { Provider as TrackProvider } from './src/context/TrackContext';
import { Ionicons } from '@expo/vector-icons';

const trackListFlow = createStackNavigator({
    TrackList: TrackListScreen,
    TrackDetail: TrackDetailScreen
});

trackListFlow.navigationOptions = {
    title: 'Tracks',
    tabBarIcon: <Ionicons name="ios-list" size={24} color="black" />
};

const switchNavigator = createSwitchNavigator({
    // Flashscreen before mainFlow is loaded
    // on local sign in through token in AsyncStorage
    LoadScreen: LoadingScreenAuth,
    // Group of screens - no authentication
    loginFlow: createStackNavigator({
        Signup: SignupScreen,
        Signin: SigninScreen
    }),

    // Flow to show after authentication
    mainFlow: createBottomTabNavigator({
        trackListFlow,
        TrackCreate: TrackCreateScreen,
        Account: AccountScreen
    })
});

const App = createAppContainer(switchNavigator);

export default () => {
    // Order of wrapping providers does not matter
    return (
        <LocationProvider>
            <TrackProvider>
                <AuthProvider>
                    <App ref={(navigator) => {setNavigator(navigator)}} />
                </AuthProvider>
            </TrackProvider>
        </LocationProvider>
    );
};
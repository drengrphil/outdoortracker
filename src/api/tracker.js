import axios from 'axios';
import { AsyncStorage } from 'react-native';

const instanceAxios = axios.create({
    baseURL: 'http://3b250c4f271f.ngrok.io'
});

instanceAxios.interceptors.request.use(
    async (config) => {
        const token = await AsyncStorage.getItem('token');
        if (token){
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    }, // Called automatically anytime a request is made
    (err) => {
        return Promise.reject(err);
    } // Called automatically when there is an error
);

export default instanceAxios;
import React from 'react';
import { View, StyleSheet } from 'react-native';

// children are the other components passed into
// this as props.
const Spacer = ( {children} ) => {
    return <View style={styles.spacer}>{children}</View>
};

const styles = StyleSheet.create({
    spacer: {
        margin: 15
    }
});

export default Spacer;
import { AsyncStorage } from 'react-native';
import createDataContext from './createDataContext';
import trackerApi from '../api/tracker';
import { navigate } from '../navigationRef';

const authReducer = ( state, action ) => {
    switch(action.type){
        case 'add_error':
            return { ...state, errorMessage: action.payload };
        case 'signup':
            return { errorMessage: '', token: action.payload };
        case 'signin':
            return { errorMessage: '', token: action.payload };
        case 'clear_error_message':
            return { ...state, errorMessage: ''};
        case 'signout':
            return { token: null, errorMessage:''};
        default:
            return state;
    }
};

const tryLocalSignin = dispatch => async () =>{
    const token = await AsyncStorage.getItem('token');
    if (token){
        dispatch({ type: 'signin', payload: token});
        navigate('TrackList');
    } else {
        navigate('loginFlow');
    }
}

const clearErrorMessage = dispatch => () => {
    dispatch({ type: 'clear_error_message' })
}

// QUICK example of arrow function
/*const add = (a,b) => {
    return a + b;
}*/

// or

// const add = (a,b) => a + b;

const signup = (dispatch) => {
    // returns a function
    return async ({ email, password }) => {
        // make api request to sign up with that email and password
        // if we sign up, modify our state, and say user is authenticated
        // if sign up fails, we reflect an error message
        try{
            const response = await trackerApi.post('/signup', { email, password});
            // console.log(response.data);
            await AsyncStorage.setItem('token', response.data.token);
            dispatch({ type: 'signup', payload: response.data.token });

            // navigate to main flow
            navigate('TrackList'); // or mainFlow.

        } catch (err){
            // console.log(err.response.data);
            // dispatch is always called to update our state
            dispatch({ type: 'add_error', payload: 'Something went wrong with signup'});
        }
    };
};
// OR
/*
    const signup = (dispatch) => return async ({ email, password }) => {
        // make api request to sign up with that email and password
        // if we sign up, modify our state, and say user is authenticated
        // if sign up fails, we reflect an error message
        try{
            const response = await trackerApi.post('/signup', { email, password} );
            // console.log(response.data);
            await AsyncStorage.setItem('token', response.data.token);
            dispatch({ type: 'signup', payload: response.data.token });
        } catch (err){
            // console.log(err.response.data);
            // dispatch is always called to update our state
            dispatch({ type: 'add_error', payload: 'Something went wrong with signup'});
        }
    };
*/

const signin = (dispatch) => {
    return async ({ email, password }) => {
        // Try to sign in
        // Handle signin success and update state
        // Handle failure by showing error message
        try{
            const response = await trackerApi.post('/signin', {email, password });
            await AsyncStorage.setItem('token', response.data.token);
            dispatch({ type: 'signin', payload: response.data.token});
            navigate('TrackList');
        } catch(err){
            dispatch({
                type: 'add_error',
                payload: 'Something went wrong with sign in'
            });
        }
    };
};

const signout = (dispatch) =>{
    return async () => {
        await AsyncStorage.removeItem('token');
        dispatch({ type: 'signout'});
        navigate('loginFlow');
    };
};

export const { Provider, Context } = createDataContext(
    authReducer,
    { signin, signout, signup, clearErrorMessage, tryLocalSignin},
    { token: null, errorMessage: ''}
);
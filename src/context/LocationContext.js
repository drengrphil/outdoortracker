import createDataContext from './createDataContext';

const locationReducer = ( state, action ) => {
    switch (action.type){
        case 'add_current_location':
            return { ...state, currentLocation: action.payload};
        case 'start_recording':
            return { ...state, recording: true };
        case 'stop_recording':
            return { ...state, recording: false};
        case 'add_location':
            // don't mutate original state object, take existing (...state.locations)
            // and add the new locations (action.payload) to it.
            return { ...state, locations: [...state.locations, action.payload]};
        case 'change_name':
            return { ...state, name: action.payload };
        case 'reset':
            return { ...state, name: '', locations: [] };
        default:
            return state;
    }
};

// Reset state
const resetLocationContext = dispatch => () => {
    dispatch({ type: 'reset'});
}

const changeName = dispatch => (name) => {
    dispatch({ type: 'change_name', payload: name });
};

const startRecording = dispatch => () => {
    dispatch({ type: 'start_recording' });
};

const stopRecording = dispatch => () => {
    dispatch({ type: 'stop_recording' });
};

const addLocation = dispatch => (location, recording) => {
    // console.log('Tracking User');
    dispatch({ type: 'add_current_location', payload: location });
    if (recording){
        dispatch({ type: 'add_location', payload: location})
    }
};

export const { Context, Provider } = createDataContext(
    locationReducer,
    { startRecording, stopRecording, addLocation, changeName, resetLocationContext },
    { name: '', recording: false, locations: [], currentLocation: null}
);
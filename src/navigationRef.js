import { NavigationActions } from 'react-navigation';

// Function to get access to navigator
let navigator;

export const setNavigator = (nav) => {
    navigator = nav;
};

// Allow everything else to trigger navigation
export const navigate = (routeName, params) => {
    navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params
        })
    );
};
// Initial or loading screen
// Resolve user authentication status
import React, { useEffect, useContext } from 'react';
import { Context as AuthContext } from '../context/AuthContext';

const LoadingScreenAuth = () => {
    const { tryLocalSignin } = useContext(AuthContext);

    useEffect(() => {
        tryLocalSignin();
    }, []);
    return null;
};

export default LoadingScreenAuth;
import React, { useContext } from 'react';
import { View, StyleSheet } from 'react-native';
import { NavigationEvents } from 'react-navigation';
import AuthForm from '../components/AuthForm';
import NavLink from '../components/NavLink';
import { Context } from '../context/AuthContext';

// <NavigationEvents onWillFocus={clearErrorMessage}
const SigninScreen = ( { navigation } ) => {
    const { state, signin, clearErrorMessage } = useContext(Context);
    return (
        <View style={styles.container}>
            <NavigationEvents 
                onWillFocus={clearErrorMessage}
            />

            <AuthForm 
                headerText="Sign In to your Account"
                errorMessage={state.errorMessage}
                submitButtonText="Sign In"
                onSubmit={signin}
            />

            <NavLink
                linkText="Don't have an account? Signup here."
                routeName="Signup"
            />
        </View>
    );
};

// Hide header since there is a link
// to go back to sign up.
SigninScreen.navigationOptions = () => {
    return {
      headerShown: false,
    };
};

const styles = StyleSheet.create({
    container: {
        // borderColor: 'red',
        // borderWidth: 5,
        flex: 1,
        justifyContent: 'center',
        // marginBottom: 200
    },
});

export default SigninScreen;
import '../_mockLocations'; // Simulates movement on map, longitude/latitude can be changed to current location
import React, { useContext, useCallback } from 'react';
import { StyleSheet } from 'react-native';
import { Text } from 'react-native-elements';
import { SafeAreaView, NavigationEvents, withNavigationFocus } from 'react-navigation';
import Map from '../components/Map';
import { Context as LocationContext } from '../context/LocationContext';
import useLocation from '../hooks/useLocations';
import TrackForm from '../components/TrackForm';
import { Fontisto } from '@expo/vector-icons';

const TrackCreateScreen = ( { isFocused} ) => {
    const { state: { recording }, addLocation } = useContext(LocationContext);
    // Callback for resolving issues with useEffect not
    // rendering with old value of recording in memory.
    const callback = useCallback(location => {
            addLocation(location, recording);
    }, [recording]); // Variable to watch, decides if new callback function is needed.

    const [err] = useLocation(isFocused || recording, callback);
    return (
        <SafeAreaView forceInset={{ top: 'always'}}>
            <Text h4> Create Track</Text>
            <Map />
            {/* <NavigationEvents onWillBlur={() => console.log('LEAVING')} /> */}
            {err ? <Text h5>Please enable location services</Text> : null}
            <TrackForm />
        </SafeAreaView>
    );
};

TrackCreateScreen.navigationOptions = {
    title: 'Add Track',
    tabBarIcon: <Fontisto name="plus-a" size={20} color="black" />
};

const styles = StyleSheet.create({});

export default withNavigationFocus(TrackCreateScreen);